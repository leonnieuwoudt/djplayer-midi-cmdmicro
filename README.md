Behringer CMD Micro DJ Controller MIDI maps for DJ Player
=========================================================

Requires [DJ Player 7.5](https://www.facebook.com/djplayerapp/ "DJ Player")

MIDI maps
---------

* `Behringer CMD Micro DJ Controller - 2 Deck, No Mixer.djpmap`

	MIDI map setup for 2 Deck, No Mixer setup.

*WORK IN PROGRESS*

This midimap is licensed under MIT. Use at own risk.

DJ Player is Copyright (C) iMerc Ltd.
  * iTunes - https://itunes.apple.com/app/dj-player/id339810085 
  * Facebook - https://www.facebook.com/djplayerapp/
